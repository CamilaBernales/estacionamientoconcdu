/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDAO;

/*
 *
 * @author PERSONAL
 */

import ClasesDTO.AbonoPropietario;
import Hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AbonoPropietarioDAO  {
    
    private Session session; 
    private Transaction tx;

    
    
 public void iniciaOperacion() throws HibernateException {
session = HibernateUtil.getSessionFactory().openSession();
tx = session.beginTransaction();

}
 
 public void manejaException(HibernateException he) throws HibernateException{
     tx.rollback();
     throw new HibernateException("ocurrio un error en la capa de acceso a datos", he);
 }
     public int guardarAbono (AbonoPropietario abonoPropietario) throws HibernateException{
         int id = 0;
         try{
             iniciaOperacion();
             id = (int) session.save(abonoPropietario);
             tx.commit();
             
         }catch(HibernateException he){
             manejaException(he);
             throw he;
           
         }finally{
         session.close();
     }
         return id;
  
 }
     public void actualizarAbono(AbonoPropietario abonoPropietario ) throws HibernateException{
     try{
         iniciaOperacion();
         session.update(abonoPropietario);
         tx.commit();
         
     }catch(HibernateException he){
         manejaException(he);
         throw he;
         
     }finally{
         session.close();
         
     }
     }
     
      public void eliminarAbono(AbonoPropietario abonoPropietario ) throws HibernateException{
          try 
        { 
            iniciaOperacion(); 
            session.delete(abonoPropietario); 
            tx.commit(); 
        } catch (HibernateException he) 
        { 
            manejaException(he); 
            throw he; 
        } finally 
        { 
            session.close(); 
        } 
        
    }  
      public AbonoPropietario obtenAbono( int idAbono) throws HibernateException{
          AbonoPropietario abonoPropietario = null;
          try{
              iniciaOperacion();
              abonoPropietario= (AbonoPropietario) session.get(AbonoPropietario.class, idAbono );
          }finally{
              session.close();
          }
          return abonoPropietario;
      }
      
       public List<AbonoPropietario> obtenListaAbono() throws HibernateException 
    { 
        List<AbonoPropietario> listaAbono = null;  

        try 
        { 
            iniciaOperacion(); 
            listaAbono = session.createQuery("from AbonoPropietario").list(); 
        } finally 
        { 
            session.close(); 
        }  

        return listaAbono; 
    }  

          
      }
         
         
     
